import { Component } from '@angular/core';
import { InAppBrowser } from 'ionic-native';
import { NavController } from 'ionic-angular';
//import {  } from 'ionic/ionic'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {
    
  }


Open(url){
   let browser = new InAppBrowser(url);
   browser.show();
  
}
}
